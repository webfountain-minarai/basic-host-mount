ARG NODE_VERSION=14.3.0-alpine3.11
ARG PORT=3000

FROM node:$NODE_VERSION

EXPOSE $PORT

RUN mkdir /opt/node_app && chown node:node /opt/node_app
WORKDIR /opt/node_app
COPY . .

USER node
CMD ["node", "./bin/www"]
