const path = require('path');
const fs = require('fs');
const fsPromises = fs.promises;


const configDir = path.resolve('configFiles');
let configNum = 1;

async function writeConfigs(contents) {
  const fileName = `config_${configNum}.json`;
  const toSave = { name: fileName, ...contents };

  await fsPromises.writeFile(`${configDir}/${fileName}`, JSON.stringify(toSave), 'utf8');

  configNum++;
  return fileName;
}

async function postConfigurations(req, res) {
  let reqBody = [];

  req.on('data', chunk => reqBody.push(chunk));
  req.on('end', async () => {
    reqBody = Buffer.concat(reqBody).toString();

    const fileName = await writeConfigs(JSON.parse(reqBody));

    const resBody = JSON.stringify({
      message: `Configuration saved as: ${fileName}.json`
    });

    res.writeHead(201, {
      'Content-Length': Buffer.byteLength(resBody),
      'Content-Type': 'application/json'
    })
    res.write(resBody);
    res.end();
  });
}


module.exports = postConfigurations;
