const getConfigurations = require('./GET@configurations');
const postConfigurations = require('./POST@configurations');
const readinessProbe = require('./readinessProbe');
const notFound_404 = require('./notFound_404');


const allowedMethods = ['GET', 'POST'];
const allowedPaths = ['/configurations', '/readiness'];
const allowedGETpaths = ['/configurations', '/readiness'];
const allowedPOSTpaths = ['/configurations'];

function routes() {
  return (req, res) => {
    const { method, url } = req;

    console.group('Route Handler');
    console.info({ method, url });
    console.groupEnd();

    if (!allowedMethods.includes(method) || !allowedPaths.includes(url) ||
        (method === 'GET' && !allowedGETpaths.includes(url)) ||
        (method === 'POST' && !allowedPOSTpaths.includes(url))
    ) notFound_404(req, res);

    if (method === 'GET' && url === '/configurations') getConfigurations(req, res);

    if (method === 'POST' && url === '/configurations') postConfigurations(req, res);

    if (method === 'GET' && url === '/readiness') readinessProbe(req, res);
  };
}


module.exports = routes;
